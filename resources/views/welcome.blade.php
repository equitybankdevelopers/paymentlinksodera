
@extends('layout.app')
@section('content')
    <section class="">
        <div class="text-center">
            <h1 class="display-6">Complete Your Payment</h1>
        </div>
    </section><br>

    <div class="creditly-wrapper col-md-8 offset-md-2">
        <div id="issue" style="background-color: red"></div>

        @if(count($errors) > 0)
            {{--@foreach($errors->all() as $error)--}}
                <div class="alert alert-danger">
                    {{$errors->first()}}
                </div>
            <div id="issue" class="alert alert-danger">
                {{$errors}}
            </div>
           {{-- @endforeach--}}
        @endif

        <form method="post" action="{{url('payment')}}" class="form-group">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <h4 class="mb-3">Enter Your Details</h4>
                <div class="row">
                    <div class="form-group col-md-6 mb-3 {{ $errors->has('mobile') ? 'has-error' : '' }}">
                        <label for='mobile'>Mobile</label>
                        <input type="text" id='mobile' class = 'form-control input-medium bfh-phone' name = 'mobile' data-country = 'KE'>
                        <span class="text-danger">{{ $errors->first('mobile') }}</span>
                    </div>

                    <div class="form-group col-md-6 mb-3 {{ $errors->has('reference') ? 'has-error' : '' }}">
                        <label for='mobile'>Reference Number</label>
                        <input type="text" id='reference' class = 'form-control' name = 'reference' placeholder = 'Enter Order Ref'>
                        <span class="text-danger">{{ $errors->first('reference') }}</span>
                    </div>
                </div>

                <div class="row"> <span id="currencyVal"></span>
                    <div class="form-group col-md-6 mb-3 {{ $errors->has('amount') ? 'has-error' : '' }}">
                        <label for='mobile'>Amount</label>
                        <input type="text" id='amount' class = 'form-control' name = 'amount' placeholder = 'Enter Amount'>
                        <span class="text-danger">{{ $errors->first('amount') }}</span>
                    </div>

                    <div class="form-group col-md-6 mb-3 {{ $errors->has('currency') ? 'has-error' : '' }}">
                        <label for='mobile'>Currency</label>
                        <input type="text" id='currency' class = 'form-control' name = 'currency' disabled>
                        <span class="text-danger">{{ $errors->first('currency') }}</span>
                    </div>
                </div>

            <hr/>
            <h4 class="mb-3">Payment Details</h4>
                <div class="row">
                    <div class="form-group col-md-6 mb-3 {{ $errors->has('firstName') ? 'has-error' : '' }}">
                        <label for='mobile'>First Name</label>
                        <input type="text" id='firstName' class = 'form-control' name = 'firstName' placeholder = 'Enter First Name' >
                        <span id="spanVal" class="text-muted">First name as displayed on card</span><br/>
                        <span class="text-danger">{{ $errors->first('firstName') }}</span>
                    </div>

                    <div class="form-group col-md-6 mb-3 {{ $errors->has('lastName') ? 'has-error' : '' }}">
                        <label for='mobile'>Last Name</label>
                        <input type="text" id='lastName' class = 'form-control' name = 'lastName' placeholder = 'Enter Last Name' >
                        <span id="spanVal" class="text-muted">Last name as displayed on card</span><br/>
                        <span class="text-danger">{{ $errors->first('lastName') }}</span>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6 mb-3 {{ $errors->has('card') ? 'has-error' : '' }}">
                        <label for='mobile'>Card Number</label>
                        <input type="text" id='card' class = 'form-control number credit-card-number' name = 'card' placeholder = 'Enter Card Number' >
                        <span class="text-danger">{{ $errors->first('card') }}</span>
                    </div>

                    <div class="form-group col-md-3 mb-3 {{ $errors->has('expiration') ? 'has-error' : '' }}">
                        <label for='mobile'>Expiry Date</label>
                        <input type="text" id='expiration' class = 'form-control expiration-month-and-year' name = 'expiration' placeholder = 'MM / YY' >
                        <span class="text-danger">{{ $errors->first('expiration') }}</span>
                    </div>

                    <div class="form-group col-md-3 mb-3 {{ $errors->has('cvv') ? 'has-error' : '' }}">
                        <label for='mobile'>CVV</label>
                        <input type="text" id='cvv' class = 'form-control security-code' name = 'cvv' placeholder = '...' >
                        <span class="text-danger">{{ $errors->first('cvv') }}</span>
                    </div>
                </div>
                <hr/>

                <div class="">
                    <div class="align-middle">
                        <button id="submit" type="submit" class="btn btn-secondary btn-block">Submit</button>
                    </div>
                </div>

        </form>

    </div>
    <script type="text/javascript">
        $(function() {
            var creditly = Creditly.initialize(
                '.creditly-wrapper .expiration-month-and-year',
                '.creditly-wrapper .credit-card-number',
                '.creditly-wrapper .security-code',
                '.creditly-wrapper .card-type');

            $(".creditly-card-form .submit").click(function(e) {
                e.preventDefault();
                var output = creditly.validate();
                if (output) {
                    // Your validated credit card output
                    console.log(output);
                    console.log('here');
                }
            });
        });
    </script>
@endsection