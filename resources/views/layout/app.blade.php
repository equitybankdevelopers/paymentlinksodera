{{--

/**
 * Created by PhpStorm.
 * User: kevinknight
 * Date: 8/26/18
 * Time: 1:44 AM
 */--}}

<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Kevin Njoroge">
    <!-- CSRF_token -->
    <meta name="csrf-token" content="{{csrf_token()}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap.min.css') }}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- project css -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- jquery -->
    <script src="{{ asset('vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-formhelpers.min.js') }}"></script>
    <title>ODERA OBAR & COMPANY ADVOCATES</title>

</head>
<body>

@include('inc.navbar')

<div class="container">
    <div class="align-content-center">
        @include('inc.messages')
        @yield('content')
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('vendor/popper.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('vendor/creditly.js') }}"></script>

</body>
</html>