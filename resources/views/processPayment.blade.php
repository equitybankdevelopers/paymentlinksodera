{{--
/**
 * Created by PhpStorm.
 * User: kevinknight
 * Date: 8/26/18
 * Time: 1:42 AM
 */--}}

@extends('layout.app')
@section('content')








<div class="jumbotron">
  <div class=" text-center">
    <p class="display-4 text-primary mb-3">Payment {{session('responseData')['status']}}</p>
    <p class="lead">Transaction Reference: <span class="font-weight-bold">{{session('responseData')['transactionId']}}</span></p>
  </div>
  <hr class="my-4">
  <a class="btn btn-primary btn-lg align-items-start" href="{{ url('/') }}" role="button">Back to Payments</a>
</div>
@endsection
