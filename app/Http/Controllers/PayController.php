<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Message;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Middleware;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\Facades\Redirect;

class PayController extends Controller
{
    private $base_url = 'https://api-test.equitybankgroup.com';
    private $merchantCode = '5190221782';
    private $outletCode = '4974599077';
    private $expiry = '2025-02-17T19:00:00';

    public function index(){
        return view('welcome');
    }

    public function getToken(){

        $key = base64_encode('bpTtQOGgBQfw1mkNNfUqQPqO5qUiMxE7:cDXeBXj6j8Ss0BO7');
        $client = new Client();
        $res = $client->request('POST', $this->base_url."/identity-uat/v1/token", [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Basic '.$key
            ],
            'form_params' => [
                'username' => $this->merchantCode,
                'password' =>  'JU7j5PcvjY0KisWSvgRLl8XT6vR5Fr3z',
                'grant_type' =>  'password',
                'merchant' =>  'Apigee001',
                'service' => 'ECOMMERCE'
                ]
        ]);

        $tokens = json_decode($res->getBody(),true);
        $payToken = $tokens['access_token'];
        Session::put('token', $payToken);
        $merchantData = $this->getMerchantDetails();

        $merchantChannel = Session::get('merchantDetails')["channels"];
        $ccr = explode(":", implode($merchantChannel))[1];

       return $ccr;
    }

    public function getMerchantDetails(){

        if (Session::get('token') === null || Session::get('token') === ''){
            $this->getToken();
            exit();
        }
        else {
            $client = new Client();
            $res = $client->request('GET', $this->base_url."/transaction-uat/v1/merchants/". $this->merchantCode . "/" . $this->outletCode, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '. Session::get('token')
                ]
            ]);
            $merchantData = json_decode($res->getBody(),true);
            Session::put('merchantDetails', $merchantData);
        }
        print_r($merchantData);
    }

    public function createBill(){
        if (Session::get('token') === null || Session::get('token') === ''){
            $this->getToken();
            exit();
        }
        else {
            $input = Input::all();
            $rules = array(
                'reference' => 'required|distinct',
                'amount' => 'required',
                'mobile' => 'required',
                'firstName' => 'required',
                'lastName' => 'required',
                'card' => 'required',
                'expiration' => 'required',
                'cvv' => 'required|max:4'
            );
            $validation = Validator::make($input, $rules);
            // validate incoming request

            if ($validation->fails()) {
                return redirect('/')
                    ->withErrors($validation);
            }
            else {
                $reference = Input::get('reference');
                $amount = Input::get('amount');
                $mobile = Input::get('mobile');
                $firstName = Input::get('firstName');
                $lastName = Input::get('lastName');
                $card = Input::get('card');
                $expiration = Input::get('expiration');
                $cvv = Input::get('cvv');

                $merchantChannel = Session::get('merchantDetails')["channels"];

                if (Session::get('merchantDetails') === null || Session::get('token') === '') {
                    $this->getMerchantDetails();
                    exit();
                } else {
                    $order = [
                        'customer' => [
                            'name' => Session::get('merchantDetails')["merchantName"],
                            'customerid' => $this->outletCode
                        ],
                        'order' => [
                            'channel' => explode(":", implode($merchantChannel))[0],
                            'billerCode' => explode(":", implode($merchantChannel))[1],
                            'currency' => explode(":", implode($merchantChannel))[2],
                            'reference' => $reference,
                            'amount' => $amount,
                            'expiry' => $this->expiry,
                            'description' => 'Yaya Center Card Payment UI',
                        ]
                    ];
                }

                $client = new Client();
                $res = $client->request('POST', $this->base_url . "/transaction-uat/v1/bills", [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . Session::get('token')
                    ],
                    'body' => json_encode($order)
                ]);
                $responseData = json_decode($res->getBody(), true);

                if ($responseData['status'] == false){
                     $errors = $responseData['remarks'];
                    return view('welcome')->with([$errors]);
                }
                else{

                Session::put('createBillRes', $responseData);
                $CardResponse = $this->cardPayment($mobile, $card, $expiration, $cvv, $firstName, $lastName);
               // dd($CardResponse);

                if (array_key_exists('status', $CardResponse) && $CardResponse['status'] == 'Success' && array_key_exists('transactionId', $CardResponse) && $CardResponse['transactionId'] != '') {
                    return redirect('processPayment')->with(['responseData' => $CardResponse]);

                } elseif ($CardResponse['error'] == 'bad_request' && $CardResponse['code'] == '400.02.001'){
                    $error = $CardResponse['message'];
                    return Redirect::back()->withErrors([$error]);

                } else{
                   // dd('System Error');
                    $error = 'System Error, Please try again!';
                    return Redirect::back()->withErrors([$error]);
                }
                }
            }
        }
    }

    public function cardPayment($mobile, $card, $expiration, $cvv, $firstName, $lastName){

        if (Session::get('token') === null || Session::get('token') === ''){
            $this->getToken();
            exit();
        }
        else {
            $merchantChannel = Session::get('merchantDetails')["channels"];
            $cardNum = str_replace(' ', '', $card);
            $expiring = explode(' / ', $expiration);
            $newExpiring = $expiring[1] . $expiring[0];

            $order = [
                'customer' => [
                    "name" => $firstName . $lastName,
                    "customerid" => $this->outletCode,
                    "mobileNumber" => $mobile
                ],
                'card' => [
                    "number" => $cardNum,
                    "expiry" => $newExpiring,
                    "securityCode" => $cvv
                ],
                'transaction' => [
                    "orderRef" => Session::get('createBillRes')['orderRef'],
                    "reference" => Session::get('createBillRes')['reference'],
                    "amount" => Session::get('createBillRes')['amount'] * 100,
                    "currency" => explode(":", implode($merchantChannel))[2],
                    "billerCode" => explode(":", implode($merchantChannel))[1],
                    "orderExpiry" => Carbon::now()->format('Y-m-d\TH:i:s+00:00'), // TO CHANGE
                    "date" => Carbon::now()->format('Y-m-d\TH:i:s'),
                    "valueDate" => Carbon::now()->format('Y-m-d\+00:00'),
                    "postedDate" => Carbon::now()->format('Y-m-d\TH:i:s'),
                    "description" => 'Payment Order'
                ]
            ];

            try {

                $client = new Client(['http_errors'=>false]);
                $res = $client->request('POST', 'https://uat.jengahq.io/transaction/v2/migs/checkout', [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer ' . Session::get('token')
                    ],
                    'body' => json_encode($order)
                ]);

                $responseData = json_decode($res->getBody(), true);
                return $responseData;
            }catch (ClientException $exception){
                $errorResp = json_decode($exception->getMessage());
                return $errorResp;
            }

            /* if ($responseData['status'] == 'Success' && $responseData['transactionId'] != '' || $responseData['transactionId'] != null){

                 return $responseData;

             } elseif ($responseData['error'] == 'bad_request' && $responseData['code'] == '400.02.001') {
                 $errorMsg = explode(":", implode($responseData['message']))[2];
                // $dispaly = explode(":", implode($responseData['message']))[2];
                return $errorMsg;

             }*/
        }
    }

}